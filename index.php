<?php require_once 'phpfucnstaff/headerInt.php';
if(isset($_SESSION["userInfo"])){
    header("location: outletSet.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<?php require_once 'head.php';?>
<style>
body {
    /* background: rgb(203, 253, 255);
    background: linear-gradient(180deg, rgba(203, 253, 255, 1) 40%, rgba(255, 255, 255, 1) 100%); */
    background-color: #FFF;
    background-size: 100% 100%;
    background-position: center;
    background-repeat: no-repeat;
}
</style>

<body class=" vh-100 vw-100">
    <?php //require_once 'components/loadingIndicator.php';?>
    <main>
        <div class="container">
            <div class="row">
                <div
                    class="col-lx-12 col-lg-12 col-md-12 col-sm-12 d-flex flex-column align-items-center justify-content-center vh-100">
                    <img src="asset/image/logo.png" class="img-fluid" alt="ncaLogo">
                    <h3 class="mt-2"><span style="color:#2c2a68;">i</span><span style="color:#0872b9;">Service</span>
                    </h3>
                    <form action="API/proxy.php?method=checkAuthen" method="POST" onsubmit="showLoadInt();">
                        <div class="row d-flex flex-column align-items-center justify-content-center">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="username">ชื่อผู้ใช้</label>
                                    <input type="text" class="form-control shadow-sm" id="username" name="username" autocomplete="off" value="" required>                            
                                </div>
                                <div class="form-group">
                                    <label for="password">รหัสผ่าน</label>
                                    <input type="password" class="form-control shadow-sm" id="password" name="password" autocomplete="off" value="" required>                            
                                </div>
                                <div class="w-100 text-right mt-2">
                                    <a href="#" onclick="forgetPassword();" hidden>ลืมรหัสผ่าน</a>
                                    <a href="testBarcode.html" rel="noopener noreferrer" hidden>BarcodeScaner</a>
                                </div>
                                <button class="btn btn-primary btn-block shadow-sm mt-3" type="submit"><i class="bi bi-box-arrow-in-right"></i> เข้าสู่ระบบ</button>
                                <div class="w-100 text-center">
                                <p class="mt-5 text-muted pe-none">VERSiON::inDev</p>
                                </div>
                                <div>
                                <?php
                        if($_GET["authStatus"] == "incorrect"){
                            echo '<div class="alert alert-warning mt-3 shadow-sm" role="alert">
                            ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง
                          </div>';
                        }
                    ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <?php
require_once 'scripts.php';
//require_once 'components/footer.php';
?>
    <script>
    $(document).ready(function() {
        // handleScriptLoad();
    });
    </script>
</body>

</html>
<footer class="mt-4 customFooter fixed-bottom">
    <div class="container mt-2">
        <div class="row">
            <div class="col-3 text-center">
                <img src="asset/image/seat-empty.png" class="img-fluid seatStatusImg" alt="seatEmpty">
                <p class="mt-2">ว่าง</p>
            </div>
            <div class="col-3 text-center">
                <img src="asset/image/seat-booked.png" class="img-fluid seatStatusImg" alt="seatBooked">
                <p class="mt-2">มีผู้โดยสาร</p>
            </div>
            <div class="col-3 text-center">
                <img src="asset/image/seat-checked.png" class="img-fluid seatStatusImg" alt="seatChecked">
                <p class="mt-2">ตรวจสอบแล้ว</p>
            </div>
            <div class="col-3 text-center">
                <img src="asset/image/seat-absend.png" class="img-fluid seatStatusImg" alt="seatAbsend">
                <p class="mt-2">ไม่มา</p>
            </div>
        </div>
    </div>
</footer>
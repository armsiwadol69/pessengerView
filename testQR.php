<!DOCTYPE html>
<html lang="en">
<?php require_once 'head.php';?>

<body>
    <div class="contanier">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Barcode Test 101</h1>
                <video muted autoplay id="video" src="#">

                </video>
                <h1>Barcode Test 101</h1>
                <p id="debugText"></p>
            </div>
        </div>
    </div>
    <?php require_once 'scripts.php';?>
    <script>
    $(document).ready(function() {
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;

        if (navigator.getUserMedia) {
            navigator.getUserMedia({
                    audio: true,
                    video: {
                        width: 1280,
                        height: 720
                    }
                },
                function(stream) {
                    var video = document.querySelector('video');
                    video.src = window.URL.createObjectURL(stream);
                    video.onloadedmetadata = function(e) {
                        video.play();
                    };
                },
                function(err) {
                    console.log("The following error occured: " + err.name);
                }
            );
        } else {
            console.log("getUserMedia not supported");
        }
    });
    </script>
</body>

</html>
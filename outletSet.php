<?php 
    require_once 'phpfucnstaff/headerInt.php';
    extract($_SESSION["userInfo"]);
?>
<!DOCTYPE html>
<html lang="en">
<?php require_once 'head.php';?>
<body class=" vh-100 vw-100">
    <?php require_once 'components/loadingIndicator.php'?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-lx-12 col-lg-12 col-md-12 col-sm-12">
                    <h6 class="mt-2 text-primary">ยินดีต้อนรับ</h6>
                    <h4 class="font-weight-bold">คุณ<?php echo $staff_name; ?></h4>
                    <hr>
                    <div id="outletSetDiv">
                        <h4>โปรดเลือกสาขาปฏิบัติงาน</h4>
                        <form action="getTimetable.php" method="GET" onsubmit="showLoadInt();">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="user_outlet">สาขา</label>
                                        <select class="form-control selectpicker shadow-sm show-tick" data-style="btn-outline-info" data-live-search="false" name="user_outlet" id="user_outlet" required>
                                            <option value="" selected>โปรดเลือก...</option>
                                            <?php
                                                foreach($outletset as $m_outlet => $option) {
                                                    echo '<option value='.$option->m_outlet.','.$option->m_outlet_name.'>'.$option->m_outlet_name.'</option>';
                                                }
                                            ?>
                                        </select>                          
                                    </div>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-outline-danger shadow-sm mt-3 mx-0 w-100" type="button" onclick="confirmLogout();"><i class="bi bi-box-arrow-left"></i> ออกจากระบบ</button>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-primary shadow-sm mt-3 mx-0 w-100" type="submit"><i class="bi bi-check2"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php
require_once 'scripts.php';
//require_once 'components/footer.php';
?>
    <script>
        <?php
        $name = $staff_name;
        $nameArray = explode(' ', $name);
        $firstName = $nameArray[0];
        ?>
    sessionStorage.setItem('user_name', '<?php echo $firstName;?>')
    sessionStorage.setItem('staff_id', '<?php echo $staff_id;?>')
    $(document).ready(function() {
        handleScriptLoad();
    });
    </script>
</body>

</html>
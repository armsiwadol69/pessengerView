<?php require_once 'phpfucnstaff/headerInt.php';?>
<!DOCTYPE html>
<html lang="en">
<?php require_once 'head.php';?>

<body>
    <style>
    .swiper-slide {
        width: 50 vw;
    }

    body {
        padding-left: 15px !important;
        padding-right: 15px !important;
    }
    </style>
    <?php require_once 'components/loadingIndicator.php'?>
    <script>

    </script>
    <main>
        <div class="container p-0">
            <div class="row">
                <div class="col-12 p-0" hidden>
                    <div class="btn-group w-100" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-lg btn-primary w-50 rounded-0"
                            onclick="changeView(this.id)" id="overviewBtn">แผนผังรถ</button>
                        <button type="button" class="btn btn-lg btn-primary w-50 rounded-0"
                            onclick="changeView(this.id)" id="passengerDetailsBtn">รายละเอียดผู้โดยสาร</button>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-12 col-xl-6" hidden>
                    <div class="input-group my-2">
                        <select class="form-control" name="busType" id="busType" disabled>
                            <option value="" selected>Just Press Get Data Button</option>
                            <option value='["GOL",30]'>GOLD / 30 Seats</option>
                            <option value='["GOL",31]'>GOLD / 31 Seats</option>
                            <option value='["GOL",32]'>GOLD / 32 Seats</option>
                            <option value='["FIR",23]'>FIRST / 23 Seats</option>
                            <option value='["FIR",30]'>FIRST / 30 Seats</option>
                        </select>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-outline-primary" onclick="loadBusData();">Get
                                Passengers Data</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-12 col-xl-6" hidden>
                    <button type="button" class="btn btn-outline-secondary my-2 btn-block"
                        onclick="countSeatDivs();">Get
                        Total Seat</button>
                </div>
                <div class="col-12" style="width:100%">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="max-height:100dvh;overflow-y: scroll;overflow-x:hidden;">
                                <div class="p-2">
                                    <form action="javascript:void(0);">
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="taxDes">เลขที่แท็กซ์ <span
                                                            class="font-weight-bold">ใบแรกลงปลายทาง</span></label>
                                                    <input type="number" class="form-control" name="taxDes" id="taxDes"
                                                        placeholder="ปลายทาง" value="0">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="taxInBetween">เลขที่แท็กซ์ <span
                                                            class="font-weight-bold">ใบแรกลงระหว่างทาง</span></label>
                                                    <input type="number" name="taxInBetween" class="form-control"
                                                        id="taxInBetween" placeholder="ระหว่างทาง" value="0">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-12">
                                                <button type="button" class="btn btn-danger w-100 my-1" id="noTaxBtn"
                                                    onclick="noTaxNumberStart();">ไม่มีเลขแท๊กซ์</button>
                                            </div>
                                            <div class="col-lg-4 col-sm-12">
                                                <button type="button" class="btn btn-success w-100 my-1 disabled"
                                                    onclick="haveTaxNumberStart();" id="haveTaxBtn" disabledx><i
                                                        class="bi bi-tag"></i> บันทึกเพื่อไปตรวจเลขแท๊กซ์</button>
                                            </div>
                                            <div class="col-lg-4 col-sm-12">
                                                <button class="btn btn-success btn-block shadow-sm my-1 disabled"
                                                    disabled type="button" id="closeTaxBtn" data-toggle="modal"
                                                    data-target="#closeTax" onclick="lossTimeMemory();" hidden><i
                                                        class="bi bi-tag"></i> กรอกเลขแท็กซ์ปิดงาน</button>
                                            </div>
                                            <div class="col-lg-4 col-sm-12">
                                                <button class="btn btn-secondary btn-block shadow-sm my-1" type="button"
                                                    onclick="history.back();" id="backBtn"><i
                                                        class="bi bi-arrow-bar-left"></i> กลับ</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr>
                                <h4 class="mt-1 ml-2">ข้อมูลเที่ยวรถและผู้โดยสาร</h4>
                                <table class="passengerSummary w-100 ml-2">
                                    <tr>
                                        <td>รถ : </td>
                                        <td id="busnameText"></td>
                                    </tr>
                                    <tr>
                                        <td>ปลายทาง : </td>
                                        <td id="destName"></td>
                                    </tr>
                                    <tr>
                                        <td>วันที่ : </td>
                                        <td id="dateText"></td>
                                    </tr>
                                    <tr>
                                        <td>เวลารถออก : </td>
                                        <td id="leaveTimeText"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr class="w-75">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนผู้โดยสารทั้งหมด : </td>
                                        <td id="totalPassenger"></td>
                                    </tr>
                                    <tr>
                                        <td>ตั๋วขึ้นต้นทาง : </td>
                                        <td id="passengerFrom"></td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนผู้โดยสารลง : </td>
                                        <td id="passengerDown"></td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนผู้โดยสารขึ้นจริง : </td>
                                        <td id="passengerPresent"></td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนผู้โดยสารไปต่อ : </td>
                                        <td id="passengerGoing"></td>
                                    </tr>
                                </table>
                                <span class="">
                                    <hr class="mtb">
                                </span>
                            </div>
                            <div class="swiper-slide" style="max-height:100dvh;overflow-y: scroll;overflow-x:hidden;">
                                <div class="swiper-zoom-container">
                                    <div class="swiper-zoom-target" id="busTemplate">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="w-100" style="max-height:100dvh;overflow-y: scroll;" id="passengerDetails">
                                    <ul class="list-group mt-3" id="passengerList"></ul>
                                    <!-- <span style="margin-top: 7rem;"></span> -->
                                    <span class="">
                                        <hr class="mtb">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="swiper-button-prev" id="sbp" hidden></div>
                        <div class="swiper-button-next" id="sbn" hidden></div> -->
                        <!-- <div class="swiper-pagination"></div> -->
                    </div>
                </div>
            </div>
            <!-- <div class="col-12" style="margin-top: 40px;"></div> -->
        </div>


        <footer>
            <div class=" fixed-bottom bg-dark">
                <div class="row no-gutters" id="bottomMenu">
                    <div class="col-8">
                        <input type="number" class="form-control h-100 rounded-0" id="tnInputF"
                            onfocus="this.value='';this.removeAttribute('readonly');" readonly
                            onkeyup="findPassengerByTiketNo(this.value);" placeholder="สแกนบาร์โค้ดเลขที่ตั๋ว..."
                            required readonly>
                        <input type="hidden" id="inputDecoly" readonly>
                    </div>
                    <div class="col-6" hidden>
                        <button class="btn btn-block btn-primary btn-lg rounded-0 disabled" disabled><i
                                class="bi bi-camera"></i></button>
                    </div>
                    <div class="col-4"><button class="btn btn-block btn-primary rounded-0"
                            onclick="callFlutterScaner();"><i class="bi bi-upc-scan mt-1"></i></button>
                    </div>
                    <div class="col-4"><button class="btn btn-block btn-secondary rounded-0 slide-button"
                            data-slide="0">ข้อมูลเที่ยว</button>
                    </div>
                    <div class="col-4"><button class="btn btn-block btn-info rounded-0 slide-button"
                            data-slide="1" checked>ผังที่นั่ง</button>
                    </div>
                    <div class="col-4"><button class="btn btn-block btn-info rounded-0 slide-button"
                            data-slide="2">ข้อมูล ผดส.</button>
                    </div>
                </div>
            </div>
        </footer>


        </div>
        <div class="openScanerBtn" hidden>
            <button type="button" class="btn round" id="scanBtn" onclick="readBarcode();" hidden>
                <i class="bi bi-upc-scan mt-1"></i></button>
        </div>
    </main>

    <div class="modal fade" id="closeTax" tabindex="-1" role="dialog" aria-labelledby="CloseTax" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="CloseTaxTitle">กรอกเลขแท๊กซ์เพื่อปิดงาน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="javascript:void(null);" name="frm" id="frm" method="POST"
                        onsubmit='toastLoading("กำลังบันทึกข้อมูล...");sentCloseTaxFrom(event);'>

                        <input type="hidden" name="txt_busline" id="txt_busline" value="">
                        <input type="hidden" name="txt_buslinetype" id="txt_buslinetype" value="">
                        <input type="hidden" name="txt_date" id="txt_date" value="">
                        <input type="hidden" name="txt_srctime" id="txt_srctime" value="">
                        <input type="hidden" name="txt_busname" id="txt_busname" value="">
                        <input type="hidden" name="txt_leavedatetime" id="txt_leavedatetime" value="">
                        <input type="hidden" name="txt_locksumid" id="txt_locksumid" value="">
                        <input type="hidden" name="txt_getCltim" id="txt_getCltim" value="">
                        <input type="hidden" name="txt_empname" id="txt_empname" value="">
                        <input type="hidden" name="txt_empid" id="txt_empid" value="">
                        <input type="hidden" name="txt_outlet" id="txt_outlet" value="">

                        <div style="font-size: 18px;text-align: center;">เลขที่ Tax ลงปลายทาง</div>
                        <table class="table table-bordered">
                            <tr>
                                <td style="font-size: 16px;">ใบแรก</td>
                                <td><input type="number" onkeyup="checkNumber(this);" name="txt_tax1" id="txt_tax1"
                                        placeholder="ใบแรก" value="" class="form-control"></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบสุดท้าย</td>
                                <td><input type="number" onkeyup="calulat(txt_tax1.value,this.value);checkNumber(this);"
                                        name="txt_last1" id="txt_last1" placeholder="ใบสุดท้าย" class="form-control"
                                        required></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใช้ไป</td>
                                <td><input type="number" name="txt_us1" id="txt_us1" onkeyup="checkNumber(this);"
                                        placeholder="ใช้ไป" class="form-control" value="" required readonly></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบแรก 2</td>
                                <td><input type="number" value="0" name="txt_tax2"
                                        onkeyup="checkinputrequs('txt_last2');checkNumber(this);" id="txt_tax2"
                                        placeholder="ใบแรก 2" class="form-control" required></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบสุดท้าย 2</td>
                                <td><input type="number" value="0"
                                        onkeyup="calulat2(txt_tax2.value,this.value);checkNumber(this);"
                                        name="txt_last2" id="txt_last2" placeholder="ใบสุดท้าย 2" class="form-control"
                                        required></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใช้ไป 2</td>
                                <td><input type="number" name="txt_us2" id="txt_us2" value=""
                                        onkeyup="checkNumber(this);" placeholder="ใช้ไป 2" class="form-control" required
                                        readonly></td>
                            </tr>
                        </table>
                        <hr>
                        <!-- //2 -->
                        <div style="font-size: 18px;text-align: center;">เลขที่ Tax ระหว่างทาง</div>
                        <table class="table table-bordered">
                            <tr>
                                <td style="font-size: 16px;">ใบแรก</td>
                                <td><input type="number" name="txt_s_tax1" id="txt_s_tax1" onkeyup="checkNumber(this);"
                                        placeholder="ใบแรก" value="" class="form-control"></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบสุดท้าย</td>
                                <td><input type="number"
                                        onkeyup="calulat3(txt_s_tax1.value,this.value);checkNumber(this);"
                                        name="txt_s_last1" id="txt_s_last1" placeholder="ใบสุดท้าย" class="form-control"
                                        <?php echo $required; ?>></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใช้ไป</td>
                                <td><input type="number" value="" name="txt_s_use1" id="txt_s_use1"
                                        onkeyup="checkNumber(this);" placeholder="ใช้ไป" class="form-control" readonly
                                        <?php echo $required; ?>></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบแรก 2</td>
                                <td><input type="number" value="0"
                                        onkeyup="checkinputrequs('txt_s_last2');checkNumber(this);" name="txt_s_tax2"
                                        id="txt_s_tax2" placeholder="ใบแรก2" class="form-control" required></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใบสุดท้าย 2</td>
                                <td><input type="number" value="0"
                                        onkeyup="calulat4(txt_s_tax2.value,this.value);checkNumber(this);"
                                        name="txt_s_last2" id="txt_s_last2" placeholder="ใบสุดท้าย 2"
                                        class="form-control" required></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">ใช้ไป 2</td>
                                <td><input type="number" name="txt_s_use2" id="txt_s_use2" onkeyup="checkNumber(this);"
                                        value="" placeholder="ใช้ไป 2" class="form-control" required readonly></td>
                            </tr>
                        </table>
                        <hr>
                        <div style="font-size: 18px;text-align: center;">รถเสียเวลา (รถออกช้า)</div>
                        <table class="table table-bordered">
                            <tr>
                                <td style="font-size: 16px;">ใส่หมายเหตุ</td>
                                <td><textarea name="txt_comment" id="txt_comment" row="5"
                                        class="form-control"></textarea></td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px;">รถออกช้า (นาที)</td>
                                <td><input type="text" name="txt_miniter" id="txt_miniter" placeholder="นาน (นาที)"
                                        value="" class="form-control" readonly></td>
                            </tr>
                        </table>
                        <hr>
                        <div>
                            <input type="checkbox" name="txt_isclose" id="txt_isclose" value="1" required>
                            &nbsp;&nbsp;&nbsp;&nbsp;<label
                                for="txt_isclose">*ติ๊กถูกเมื่อจบคีย์เลขแท็กซ์แล้วเท่านั้น</label>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 my-1">
                                <button type="submit" class="btn btn-raised btn-success btn-block h-100">
                                    กดเพื่อยืนยันแท็กซ์</button>
                            </div>
                            <div class="col-6 my-1">
                                <button type="button" class="btn btn-raised btn-danger btn-block h-100"
                                    data-dismiss="modal">ย้อนกลับ</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <?php
require_once 'scripts.php';
//require_once 'components/footer.php';
?>
    <script>
    $(document).ready(function() {
        sessionStorage.setItem('busline', '<?php echo $_GET["busline"] ?>');
        sessionStorage.setItem('buslinetype', '<?php echo $_GET["buslinetype"] ?>');
        sessionStorage.setItem('date', '<?php echo $_GET["date"] ?>');
        sessionStorage.setItem('srctime', '<?php echo $_GET["srctime"] ?>');
        sessionStorage.setItem('outlet', '<?php echo $_GET["outlet"] ?>');
        sessionStorage.setItem('stpoint', '<?php echo $_GET["stpoint"] ?>');
        sessionStorage.setItem('busname', '<?php echo $_GET["busname"] ?>');
        sessionStorage.setItem('scrtimeConverted', convertTime('<?php echo $_GET["srctime"] ?>'));
        sessionStorage.setItem('bustype', '<?php echo $_GET["bustype"] ?>');
        checkFirstTagno();
        resultOfqueue();
        loadBusData().then(function() {
            handleScriptLoad();
            intSwiper();
            document.getElementById("busnameText").innerHTML = sessionStorage.getItem(
                'busname');
            document.getElementById("dateText").innerHTML = sessionStorage.getItem('date');
            document.getElementById("leaveTimeText").innerHTML = sessionStorage.getItem(
                'scrtimeConverted');
        });
    });
    document.addEventListener('keydown', (event) => {
        var keyName = event.key;
        var keyCode = event.code;
        var charCode = event.charCode;
        console.log(keyName);
        if (sessionStorage.getItem("isTaxSet") === "1") {
            if (!Swal.isVisible()) {
                if (keyName == "Unidentified") {
                    document.getElementById("tnInputF").focus();
                    //readBarcode();
                }
            }
        }

    }, false);

    function calulat(par_old, par_new) {
        if (parseInt(par_new) >= parseInt(par_old)) {
            if (parseInt(par_old) == '0' && parseInt(par_new) == '0') {
                document.getElementById('txt_us1').value = '0';
            } else {
                var calx = (par_new - par_old) + 1;
                document.getElementById('txt_us1').value = calx;
            }
        } else {
            document.getElementById('txt_us1').value = '0';
        }
    }

    function calulat2(par_old, par_new) {
        if (parseInt(par_new) >= parseInt(par_old)) {
            if (parseInt(par_old) == '0' && parseInt(par_new) == '0') {
                document.getElementById('txt_us2').value = '0';
            } else {
                var calx = (par_new - par_old) + 1;
                document.getElementById('txt_us2').value = calx;
            }
        } else {
            document.getElementById('txt_us2').value = '0';
        }
    }

    function checkinputrequs(par_id) {
        document.getElementById(par_id).required;
    }

    function calulat3(par_old, par_new) {
        if (parseInt(par_new) >= parseInt(par_old)) {
            if (parseInt(par_old) == '0' && parseInt(par_new) == '0') {
                document.getElementById('txt_s_use1').value = '0';
            } else {
                var calx = (par_new - par_old) + 1;
                document.getElementById('txt_s_use1').value = calx;
            }
        } else {
            document.getElementById('txt_s_use1').value = '0';
        }
    }

    function calulat4(par_old, par_new) {
        if (parseInt(par_new) >= parseInt(par_old)) {
            if (parseInt(par_old) == '0' && parseInt(par_new) == '0') {
                document.getElementById('txt_s_use2').value = '0';
            } else {
                var calx = (par_new - par_old) + 1;
                document.getElementById('txt_s_use2').value = calx;
            }
        } else {
            document.getElementById('txt_s_use2').value = '0';
        }
    }

    function checkNumber(elm) {
        if (isNaN(elm.value)) {
            elm.value = '';
        }
    }
    </script>
</body>

</html>
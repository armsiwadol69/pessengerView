<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <title>NCA iService : CheckLog?</title>
    <link rel="apple-touch-icon" sizes="180x180" href="asset/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="asset/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="asset/fav/favicon-16x16.png">
    <link rel="manifest" href="asset/fav/site.webmanifest">
    <link rel="mask-icon" href="asset/fav/safari-pinned-tab.svg" color="#1e74bd">
    <link rel="shortcut icon" href="asset/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="asset/fav/browserconfig.xml">
    <meta name="theme-color" content="#1e74bd">
    <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="asset/css/main.css?v=1.4">
    <link rel="stylesheet" href="asset/aos/aos.css">
    <script src="asset/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="asset/moment.js"></script>
    <link rel="stylesheet" href="asset/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="asset/bootstrap-select/css/bootstrap-select.min.css">
    <script src="asset/live.js"></script>
    <style>
    body {
        -ms-overflow-style: none;
        /* IE and Edge */
        scrollbar-width: none;
        /* Firefox */
    }

    body::-webkit-scrollbar {
        display: none;
    }
    </style>
</head>
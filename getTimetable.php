<?php 
    header('Content-Type: text/html; charset=utf-8');
    require_once 'phpfucnstaff/headerInt.php';
    extract($_SESSION["userInfo"]);
    $userOutlet = explode(',',$_GET["user_outlet"]);
    $outlet = $userOutlet[0];
    //echo $userOutlet[0];
    //echo $userOutlet[1];
    //header('Content-Type: application/json; charset=utf-8');
    $urlSrcPoint = "http://203.146.21.210/ots/funcstaff.inc.php?method=getSrcPoint&user_outlet=".$userOutlet[0];
    $srcPoint = json_decode(curlGetNca($urlSrcPoint));
    //echo $urlSrcPoint;
    //var_dump($srcPoint[0]);
    $srcMpointData = $srcPoint[0];
    $urlDesPoint = "http://203.146.21.210/ots/funcstaff.inc.php?method=getDesPoint&src_point=".$srcMpointData->m_point;
    $getAllDesPointRaw = curlGetNca($urlDesPoint);
    $desPoint = json_decode($getAllDesPointRaw);
?>
<!DOCTYPE html>
<html lang="en">
<?php require_once 'head.php';?>
<style>
body {
    /* background: rgb(203, 253, 255);
    background: linear-gradient(180deg, rgba(203, 253, 255, 1) 40%, rgba(255, 255, 255, 1) 100%); */
    background-color: #FFF;
    background-size: 100% 100%;
    background-position: center;
    background-repeat: no-repeat;
}
</style>

<body class=" vh-100 vw-100">
    <?php require_once 'components/loadingIndicator.php'?>
    <main>
        <?php
            //  echo 'stpoint : '.$srcMpointData->m_point;
            //  echo '<br>';
            //  echo 'topoint : <span id="dtopoint"></span>';
            //  echo '<br>';
            //  echo 'outlet : '.$outlet;
            //  echo '<br>';
            //  echo 'date : <span id="d_date"></span>';
        ?>
        <div class="container">
            <div class="row">
                <div class="col-lx-12 col-lg-12 col-md-12 col-sm-12">
                    <h3 class="mt-5 font-weight-bold" hidden>ยินดีต้อนรับ<br><span>คุณ<?php echo $staff_name; ?></span>
                    </h3>
                    <h6 class="mt-2">สาขาปฏิบัติงาน</h6>
                    <p class="font-weight-bold"><?php echo $userOutlet[1];?></p>
                    <hr>
                    <div id="desDiv">
                        <!-- <h4>โปรดเลือกปลายทาง</h4> -->
                        <form action="javascript:void(0);" onsubmit="getTimeTable();">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <!-- <label for="user_outlet">เลือกปลายทาง</label> -->
                                        <select class="form-control selectpicker show-tick" data-live-search="false"
                                            name="desPoint" id="desPoint" onchange="getTimeTable();" required>
                                            <option value="" selected>เลือกปลายทาง...</option>
                                            <?php
                                                foreach($desPoint as $desPoint => $option) {
                                                    echo '<option value="'.$option->m_point.'">'.$option->m_point_name.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group mt-3">
                                        <!-- <label for="user_outlet">วันที่</label> -->
                                        <input type="date" class="form-control" name="kn_date" id="kn_date">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-outline-secondary btn-block shadow-sm mt-2 mx-0" type="button" onclick="history.back();"><i class="bi bi-arrow-bar-left"></i> ย้อนกลับ</button>
                                </div>
                                <div class="col-6">
                                <button class="btn btn-primary btn-block mt-2 type="submit"><i class="bi bi-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lx-12 col-lg-12 col-md-12 col-sm-12" id="timetableDisplay" hidden>
                    <hr>
                    <table class="table table-bordered table-hover table-sm table-striped" style="background-color: #FFF;">
                        <thead>
                            <tr>
                                <th>สาย</th>
                                <th>รถ</th>
                                <th>ต้นทาง</th>
                                <th>ออก</th>
                                <th>ถึง</th>
                            </tr>
                        </thead>
                        <tbody id="timetableBody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <script>
    var outlet = '<?php echo $userOutlet[0];?>';
    var stpoint = '<?php echo $srcMpointData->m_point?>';
    //console.log("stpoint : "+stpoint);
    </script>
    <?php
require_once 'scripts.php';
//require_once 'components/footer.php';
?>
    <script>
        //sessionStorage.setItem("allDesPoint",'<?php echo $getAllDesPointRaw;?>');
        sessionStorage.setItem("userLocation",'<?php echo '["'.$userOutlet[0].'"'.','.'"'.$userOutlet[1].'"]';?>');       
        sessionStorage.setItem("user_outlet",'<?php echo $userOutlet[0];?>');                            
        $(document).ready(function() {
        let datepickerObj = document.getElementById("kn_date");
        let yesterday = moment().add(-1, 'days').format("YYYY-MM-DD");
        let today = moment().format("YYYY-MM-DD");
        let tomorrow = moment().add(1, 'days').format("YYYY-MM-DD");
        datepickerObj.value = today;
        //datepickerObj.min = yesterday;
        //datepickerObj.max = tomorrow;

        handleScriptLoad();
        // console.log("Yesterday : ",yesterday);
        // console.log("Today : ",today);
        // console.log("Tomorrow : ",tomorrow);
    });
    //fireAlertToast(outlet+" / "+stpoint);
    </script>
</body>

</html>
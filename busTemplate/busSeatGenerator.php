<div class="container-fluid w-100">
    <div class="row text-center">
        <?php
        $busType = $_GET["busType"];
        $total_seats = strval($_GET["Seats"]);
        
        if($busType == "GOL"){
            $seats_per_row = 4;
        }else if($busType == "FIR"){
            $seats_per_row = 3;
        }
    
        //echo "<div class='col-12'>Total Seats : ".$total_seats.'</div>';

        if($busType == "GOL" && ($total_seats % 2 ) == 0){
            $seats_per_row = 4;
            echo "<div class='col-12'>Seats_Per_Row : ".$seats_per_row.' BLOCK_1</div>';
            // echo '<div class="col-2 busCol"></div>
            // <div class="col-2 busCol"><p>A</p></div>
            // <div class="col-2 busCol"><p>B</p></div>
            // <div class="col-1 midlane"></div>
            // <div class="col-2 busCol"><p>C</p></div>
            // <div class="col-2 busCol"><p>D</p></div>';
            
            for ($i = 1; $i <= $total_seats; $i++) {
                $row_number = ceil($i / $seats_per_row);
                $seat_letter = chr(($i - 1) % $seats_per_row + ord('A'));
                $seat_id = "{$row_number}{$seat_letter}";
            
                // Print the busRow div at the beginning of each row
                if (($i - 1) % $seats_per_row === 0) {
                    //echo "<div class='col-1 busRow'> <p></p></div>";
                }
            
                echo "<div class='col-225p seat empty' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p class=''>{$seat_id}</p></div>";

                // Insert the midlane div after the B column aka middle row walk way
                if (($i - 1) % $seats_per_row === 1) {
                    echo "<div class='col-10p midlane'> </div>";
                }
            }
            echo '<div class="col-225p seat empty" id="ส1" onClick="getSeatData('."'"."ส1"."'".')" hidden><p>ส1</p></div>';
        }else if($busType == "GOL" && $total_seats  == 21){
            $seats_per_row = 3;
            echo "<div class='col-12'>Seats_Per_Row : ".$seats_per_row.' BLOCK_S21</div>';
            // echo '
            // <div class="col-1 busCol"></div>
            // <div class="col-3 busCol"><p>A</p></div>
            // <div class="col-2 midlane"></div>
            // <div class="col-3 busCol"><p>C</p></div>
            // <div class="col-3 busCol"><p>D</p></div>
            // ';

            $seat_letterList = array('A', 'C', 'D');

            for ($i = 1; $i <= $total_seats; $i++) {
                $row_number = ceil($i / $seats_per_row);
                //$seat_letter = chr(($i - 1) % $seats_per_row + ord('A'));
                $seat_letter = $seat_letterList[($i - 1) % $seats_per_row];
                $seat_id = "{$row_number}{$seat_letter}";
            
                // Print the busRow div at the beginning of each row
                if (($i - 1) % $seats_per_row === 0) {
                    //echo "<div class='col-1 busRow'> <p>{$row_number}</p></div>";
                }
                echo "<div class='col-30p seat empty' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p>{$seat_id}</p></div>";
                
                // Insert the midlane div after the A column
                if (($i - 1) % $seats_per_row === 0) {
                    echo "<div class='col-10p midlane'> </div>";
                }
            }
            echo '<div class="col-30p seat empty" id="ส1" onClick="getSeatData('."'"."ส1"."'".')" hidden><p>ส1</p></div>';
        }else if($busType == "GOL" && ($total_seats % 2 ) != 0){
            $seats_per_row = 4;
            $total_seats += 1;
            echo "<div class='col-12'>Seats_Per_Row : ".$seats_per_row.' BLOCK_2</div>';
            // echo '
            // <div class="col-1 busCol"></div>
            // <div class="col-2 busCol"><p>A</p></div>
            // <div class="col-1 midlane"></div>
            // <div class="col-2 busCol"><p>B</p></div>
            // <div class="col-2 busCol"><p>C</p></div>
            // <div class="col-1 midlane"></div>
            // <div class="col-2 busCol"><p>D</p></div>
            // <div class="col-1 midlane"></div>
            // ';
            
            for ($i = 1; $i <= $total_seats; $i++) {
                $row_number = ceil($i / $seats_per_row);
                $seat_letter = chr(($i - 1) % $seats_per_row + ord('A'));
                $seat_id = "{$row_number}{$seat_letter}";
            
                // Print the busRow div at the beginning of each row
                if (($i - 1) % $seats_per_row === 0) {
                    //echo "<div class='col-1 busRow'> <p>{$row_number}</p></div>";
                }
            
                if($seat_id != "8B") {
                    echo "<div class='col-225p seat empty' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p>{$seat_id}</p></div>";
                }else{
                    echo "<div class='col-225p seat empty invisible' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p>{$seat_id}</p></div>";
                }
                
                // Insert the midlane div after the A column
                if (($i - 1) % $seats_per_row === 0) {
                    echo "<div class='col-5p midlane'> </div>";
                }
                // Insert the midlane div after the C column
                if (($i - 1) % $seats_per_row === 2) {
                    echo "<div class='col-5p midlane'> </div>";
                }
                if (($i - 1) % $seats_per_row === 3) {
                    //echo "<div class='col-5p midlane'> </div>";
                }
            }
            echo '<div class="col-225p seat empty" id="ส1" onClick="getSeatData('."'"."ส1"."'".')" hidden><p>ส1</p></div>';
        }else if($busType == "FIR" && ($total_seats % 2 ) != 0){
            $total_seats += 1;
            echo "<div class='col-12'>Seats_Per_Row : ".$seats_per_row.' BLOCK_3</div>';
            // echo '
            // <div class="col-1 busCol"></div>
            // <div class="col-3 busCol"><p>A</p></div>
            // <div class="col-1 midlane"></div>
            // <div class="col-3 busCol"><p>B</p></div>
            // <div class="col-1 midlane"></div>
            // <div class="col-3 busCol"><p>C</p></div>
            // ';
            
            for ($i = 1; $i <= $total_seats; $i++) {
                $row_number = ceil($i / $seats_per_row);
                $seat_letter = chr(($i - 1) % $seats_per_row + ord('A'));
                $seat_id = "{$row_number}{$seat_letter}";
            
                // Print the busRow div at the beginning of each row
                if (($i - 1) % $seats_per_row === 0) {
                    echo "<div class='col-1 busRow'> <p>{$row_number}</p></div>";
                }
            
                
                if($seat_id != "1B") {
                    echo "<div class='col-3 seat empty' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p>{$seat_id}</p></div>";
                }else{
                    echo "<div class='col-3'></div>";
                }
                
                // Insert the midlane div after the A column
                if (($i - 1) % $seats_per_row === 0) {
                    echo "<div class='col-1 midlane'> </div>";
                }
                // Insert the midlane div after the C column
                if (($i - 1) % $seats_per_row === 1) {
                    echo "<div class='col-1 midlane'> </div>";
                }
            }
            echo '<div class="col-3 seat empty" id="ส1" onClick="getSeatData('."'"."ส1"."'".')" hidden><p>ส1</p></div>';
        }else if($busType == "FIR" && ($total_seats % 2 ) == 0){
            echo "<div class='col-12'>Seats_Per_Row : ".$seats_per_row.' BLOCK_4</div>';
            // echo '
            // <div class="col-1 busCol"></div>
            // <div class="col-3 busCol"><p>A</p></div>
            // <div class="col-2 midlane"></div>
            // <div class="col-3 busCol"><p>C</p></div>
            // <div class="col-3 busCol"><p>D</p></div>
            // ';

            $seat_letterList = array('A', 'C', 'D');

            for ($i = 1; $i <= $total_seats; $i++) {
                $row_number = ceil($i / $seats_per_row);
                //$seat_letter = chr(($i - 1) % $seats_per_row + ord('A'));
                $seat_letter = $seat_letterList[($i - 1) % $seats_per_row];
                $seat_id = "{$row_number}{$seat_letter}";
            
                // Print the busRow div at the beginning of each row
                if (($i - 1) % $seats_per_row === 0) {
                    //echo "<div class='col-1 busRow'> <p>{$row_number}</p></div>";
                }
                echo "<div class='col-30p seat empty' id='{$seat_id}' onClick='getSeatData(".'"'.$seat_id.'"'.")'> <p>{$seat_id}</p></div>";
                
                // Insert the midlane div after the A column
                if (($i - 1) % $seats_per_row === 0) {
                    echo "<div class='col-10p midlane'> </div>";
                }
            }
            echo '<div class="col-30p seat empty" id="ส1" onClick="getSeatData('."'"."ส1"."'".')" hidden><p>ส1</p></div>';
        }  
    ?>
    </div>
    <div class="row mt-1 d-flex flex-row justify-content-center text-center">
        <div class="col-12">
            <input type="hidden" name="decoly" id="decoly">
            <hr>
        </div>
        <div class="col-2 text-center">
            <img src="asset/image/seat-empty.png" class="img-fluid seatStatusImg" alt="seatEmpty">
            <p class="mt-2">ว่าง</p>
        </div>
        <div class="col-2 text-center">
            <img src="asset/image/seat-booked.png" class="img-fluid seatStatusImg" alt="seatBooked">
            <p class="mt-2">มีผู้โดยสาร</p>
        </div>
        <div class="col-2 text-center">
            <img src="asset/image/seat-checked.png" class="img-fluid seatStatusImg" alt="seatChecked">
            <p class="mt-2">ตรวจสอบแล้ว</p>
        </div>
        <div class="col-2 text-center">
            <img src="asset/image/seat-late5min.png" class="img-fluid seatStatusImg" alt="seatAbsend">
            <p class="mt-2">สายเกิน 5 นาที</p>
        </div>
        <div class="col-2 text-center">
            <img src="asset/image/seat-absend.png" class="img-fluid seatStatusImg" alt="seatAbsend">
            <p class="mt-2">ไม่มา/ยกเลิก</p>
        </div>
        <div class="col-12">
            <span class="">
                <hr class="mtb">
            </span>
        </div>
    </div>
</div>
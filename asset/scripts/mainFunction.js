function fireAlert(msg,error) {
  Swal.fire({
    icon: "error",
    title: "พบผิดพลาด",
    html: msg,
    timer: 3000,
    timerProgressBar: true,
    focusConfirm: false,
    allowEnterKey: false,
    showConfirmButton: false,
    footer: error
  })
}

function passengerAlreadyHere(msg) {
  Swal.fire({
    icon: "success",
    title: "ตรวจสอบแล้ว",
    html: msg,
    timer: 2000,
    timerProgressBar: true,
    focusConfirm: false,
    allowEnterKey: false,
    showConfirmButton: false,
  }).then(function () {
    document.getElementById("tnInputF").value = "";
    document.activeElement.blur();
    
  })
  
}

function resultRemarkPassenger(msg) {
  Swal.fire({
    icon: "success",
    title: "บันทึกสำเร็จ",
    html: msg,
    timer: 2000,
    timerProgressBar: true,
    focusConfirm: false,
    allowEnterKey: false,
    showConfirmButton: false,
  }).then(function () {
    document.getElementById("tnInputF").value = "";
    document.activeElement.blur();
  });
}

function fireTaxErr(msg) {
    Swal.fire({
        icon: "error",
        title: "พบผิดพลาด",
        html: msg,
        showConfirmButton: true,
        timer: 3000,
        timerProgressBar: true
    });
  }

function fireTaxS(msg) {
    Swal.fire({
      icon: "success",
      title: "บันทึกข้อมูลสำเร็จ",
      html: msg+'<br>กำลังกลับไปหน้าหลัก...',
      timer: 3000,
      timerProgressBar: true,
      showConfirmButton: false,
      allowOutsideClick: false
    }).then(function() {
        var localtion = JSON.parse(sessionStorage.getItem('userLocation'));
        //console.log(localtion[0]);
        window.location.href = `getTimetable.php?user_outlet=${localtion[0]},${localtion[1]}`;
    });
  }

function fireAlertInfo(msg) {
  Swal.fire({
    icon: "info",
    title: "Hi~",
    text: msg,
  });
}

function fireAlertToast(msg) {
  Swal.fire({
    icon: "info",
    title: "Infomation",
    html: msg,
    toast: true,
    timer: 3000,
    timerProgressBar: true,
    position: "center",
    showConfirmButton: false,
  });
}
function toastSuccess(msg) {
  Swal.fire({
    icon: "success",
    text: msg,
    toast: true,
    timer: 3000,
    timerProgressBar: true,
    position: "top-right",
    showConfirmButton: false,
  });
}

function toastLoading(msg) {
  Swal.fire({
    imageUrl: "asset/image/loading-37.webp",
    imageWidth: 50,
    text: msg,
    position: "center",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function memberType(memberType) {
  if (memberType === "NORMAL") {
    memberType = "-";
  } else if (memberType === "GOCARD") {
    memberType = "สมาชิก GOCARD";
  } else {
    memberType = "-";
  }
  return memberType;
}

function checkPassengerSatatus(seat_lock, seat_ratetype) {
  const statusText = [
    "<span class='text-secondary'>ว่าง<span>",
    "<span class='text-info'>ยังไม่ขึ้นรถ<span>",
    "<span class='text-success'>ตรวจสอบแล้ว<span>",
    "<span class='text-danger'>ยกเลิก<span>",
  ];
  if (seat_lock === "1") {
    return statusText[2];
  } else if (seat_ratetype === null) {
    return statusText[0];
  } else if (seat_lock === "0" && seat_ratetype === "1") {
    return statusText[1];
  } else if (seat_lock === "0" && seat_ratetype === "2") {
    return statusText[1];
  } else if (seat_lock === "0" && seat_ratetype === "3") {
    return statusText[1];
  }else {
    return statusText[3];
  }
}

function checkPassengerSuccess(seat_name, seat_dspnm) {
  Swal.fire({
    title: "ตรวจสอบผู้โดยสารแล้ว",
    text: seat_name + " : " + seat_dspnm,
    icon: "success",
    timer: 2500,
    timerProgressBar: true,
    focusConfirm: true,
    allowEnterKey: false,
  }).then(function(){
    loadBusData().then(function() {
        document.getElementById("decoly").focus();
    });
    
  }); 
}

function ticketNoLength(str) {
  if (str.length === 10) {
    return str.slice(0, 8); // Removing the last 2 characters
  } else {
    return str;
  }
}

function genPassengerDataTable(
  seat_name,
  seat_dspnm,
  seat_desnm,
  seat_srcnm,
  seat_ticket,
  seat_ratetypevar,
  seat_remark,
  seat_lock,
  seat_typenm,
  seat_ratetype
) {
  if (seat_lock === "1") {
    var btnContent = "hidden";
  }else{
    var btnContent = "";
  }

  const passengerDataTable = `
  <div style="overflow-x:hidden">
    <table class="passengerDetailTalbe w-100">
    <tr><th colspan="2">ข้อมูลผู้โดยสาร<th><tr>
    <tr>
    <td>สถานะ :</td>
    <td>${checkPassengerSatatus(seat_lock, seat_ratetype)}</td>
    </tr>
    <tr>
    <td>เลขที่ตั๋ว :</td>
    <td>${seat_ticket}</td>
    </tr>
    <tr>
    <td>ที่นั่ง :</td>
    <td> ${seat_name}</td>
    </tr>
    <tr>
    <td>ชื่อ :</td>
    <td> ${seat_dspnm}</td>
    </tr>
    <tr>
    <td>ต้นทาง :</td>
    <td> ${seat_srcnm}</td>
    </tr>
    <tr>
    <td>จุดลงรถ :</td>
    <td>${seat_desnm}</td>
    </tr>
    <tr>
    <td>ประเภท :</td>
    <td>${seat_typenm}</td>
    </tr>
    <tr>
    <tr>
    <td colspan='2'><hr></td>
    </tr>
    <tr>
    <td class='text-left' colspan='2'>${seat_remark}</td>
    </tr>
    <table>
    <div class="row p-3" ${btnContent}>
       <div class="col-6 p-1">
            <button type="button" class="btn btn-success btn-sm btn-block my-1 h-100 disabled" disabled onclick="callFlutterCamera('');"><i class="bi bi-camera"></i> ถ่ายรูป</button>
        </div>
        <div class="col-6 p-1">
            <button type="button" class="btn btn-success btn-sm btn-block my-1 h-100" autofocus onclick="lockSeatPassenger('${seat_ticket}','${seat_name}','${seat_dspnm}');"><i class="bi bi-check-circle"></i> ยืนยันตรวจสอบ</button>
        </div>
        <div class="col-6 p-1">
            <button type="button" class="btn btn-danger btn-sm btn-block my-1 h-100" onclick="remarkPassengerSatatus('${seat_ticket}','รอแล้วไม่มา','${seat_name}','${seat_dspnm}')"><i class="bi bi-clock-history"></i> รอแล้วไม่มา</button>
        </div>
        <div class="col-6 p-1">
            <button type="button" class="btn btn-warning btn-sm btn-block my-1 h-100" onclick="remarkPassengerSatatus('${seat_ticket}','ไปเที่ยวก่อนหน้าแล้ว','${seat_name}','${seat_dspnm}')"><i class="bi bi-skip-backward"></i> ไปเที่ยวก่อนหน้า</button>
        </div>
        <div class="col-12 p-1">
            <button type="button" class="btn btn-dark btn-sm btn-block my-1 h-100" onclick="swal.close();"><i class="bi bi-x-lg"></i> ปิดหน้าต่าง</button>
        </div>
    </div>
    </div>
    `;

  return passengerDataTable;
}

function confirmLogout() {
  Swal.fire({
    text: "ออกจากระบบหรือไม่?",
    icon: "question",
    showCancelButton: true,
    confirmButtonText: "ใช่",
    cancelButtonText: "ไม่",
    confirmButtonColor: "#FE0000",
  }).then((result) => {
    if (result.isConfirmed) {
      // Redirect to logout.php
      window.location.href = "phpfucnstaff/logout.php";
    }
  });
}

async function getSeatData(seatNo) {
  const passengerData = JSON.parse(sessionStorage.getItem("passengerData"));
  const psd = passengerData.filter((x) => x.seat_name === seatNo)[0]; // psd is short version of passengerData
  //console.log(psd);
  if (psd.seat_lock === "0") {
    var slBtn = true;
  } else {
    var slBtn = false;
  }
  Swal.fire({
    //icon : 'info',
    html: genPassengerDataTable(
      psd.seat_name,
      psd.seat_dspnm,
      psd.seat_desnm,
      psd.seat_srcnm,
      psd.seat_ticket,
      psd.seat_ratetypevar,
      psd.seat_remark,
      psd.seat_lock,
      psd.seat_typenm,
      psd.seat_ratetype
    ),
    showConfirmButton: false,
    showDenyButton: false,
    showCancelButton: false,
    confirmButtonText:
      '<i class="bi bi-person-check-fill"></i> บันทึกการขึ้นรถ',
    // denyButtonText: '<i class="bi bi-person-fill-x"></i> ผู้โดยสารหายตัวได้',
    cancelButtonText: "ย้อนกลับ",
    //toast: true,
    allowOutsideClick: false,
    backdrop: true,
    focusCancel:false
  }).then((result) => {
    if (result.isConfirmed) {
      loadBusData().then(() => {
        checkPassengerSuccess(psd.seat_name, psd.seat_dspnm);
      });
    } else if (result.isDenied) {
      //Swal.fire('บันทึกข้อมูลแล้ว!', 'ผู้โดยสารไม่มา', 'success')
    }
  });
  document.getElementById("tnInputF").value = "";
}
async function countPensenger(jsonData) {
  const totalPensenger = jsonData.filter(
    (x) => x.seat_ratetype !== null
  ).length;
  console.log("totalPensenger : " + totalPensenger);
  document.getElementById("totalPassenger").innerHTML =+ totalPensenger + " ท่าน";
}

async function getPassengerDataFromAPI() {
  try {
    var busline = sessionStorage.getItem("busline");
    var buslinetype = sessionStorage.getItem("buslinetype");
    var date = sessionStorage.getItem("date");
    var srctime = sessionStorage.getItem("srctime");
    var stpoint = sessionStorage.getItem("stpoint");
    var apiendpoint = `API/proxy.php?method=getAllSeat&busline=${busline}&buslinetype=${buslinetype}&date=${date}&srctime=${srctime}&outlet=${stpoint}&stpoint=${stpoint}`;
    console.log(apiendpoint);
    const response = await fetch(apiendpoint);
    const jsonData = await response.json();
  //console.log(jsonData);
  return jsonData;
  } catch (error) {
    console.log(error);
    fireAlertToast("ไม่สามารถเรียกข้อมูลผู้โดยสารได้");
    logErrorToServer("getPassengerDataFromAPI",error)
  }
}

async function loadBusData() {
  showLoadInt();
  //await new Promise(r => setTimeout(r, 1000));

  try {
    var jsonData = await getPassengerDataFromAPI();

    console.log(jsonData);
    //await timeout(1000);

    const totalSeat = jsonData.length;

    const busType = jsonData[0].bustype_var;

    document.getElementById("destName").innerHTML = jsonData[0].seat_desnm;

    console.log(busType);
    console.log(totalSeat);

    //const busType = JSON.parse(document.getElementById("busType").value);

    sessionStorage.setItem("passengerData", JSON.stringify(jsonData));

    const divBusTemplate = document.getElementById("busTemplate");
    divBusTemplate.innerHTML = "";

    //Generate bus minimap from BusType And TotalSeat
    let url =
      "busTemplate/busSeatGenerator.php?" +
      "busType=" +
      busType +
      "&Seats=" +
      totalSeat;
    const response = await fetch(url);
    const html = await response.text();

    //console.log(url);

    divBusTemplate.innerHTML = html;
    addSeatsData(jsonData);
  } catch (error) {
    handleScriptLoad();
    fireAlertToast(error);
    console.error("Error loading bus template:", error);
    logToServer("loadBusData",error)
  }
  genPassengerDataList(jsonData);
  countPensenger(jsonData);
  addMarginTopAvoid();
  handleScriptLoad();
}

//this function is for mockup, it just randomly add class to seat
function addSeatsData(jsonData) {
  //const seatDivs = document.getElementsByClassName('seat');
  const classes = ["empty", "booked", "checked", "absent"];
  for (let index = 0; index < jsonData.length; index++) {
    //const element = array[index];
    const seatTaget = document.getElementById(jsonData[index].seat_name);
    //const seatStatus = jsonData[index].seat_lock;
    const seatLock = jsonData[index].seat_lock;
    const seatRatetype = jsonData[index].seat_ratetype;
    const seatLadyseat = jsonData[index].seat_ladyseat;
    const seatTypenm = jsonData[index].seat_typenm;
    //console.log(jsonData[index].seat_name);

    if (seatLadyseat === 1) {
      seatTaget.style.backgroundColor = "#fec2d8";
      //console.log(jsonData[index].seat_name);
    }

    var isSeatlock = checkSeatStatus(seatTypenm, seatLock);
    //console.log(isSeatlock);
    seatTaget.classList.add(isSeatlock);

    if(jsonData[index].seat_name === "ส1"){
      document.getElementById("ส1").removeAttribute("hidden");
    }

    // if(seatStatus === "0"){
    //     seatTaget.classList.add(classes[0]);
    // }else if(seatStatus === "1"){
    //     seatTaget.classList.add(classes[1]);
    // }else if(seatStatus === "2"){
    //     seatTaget.classList.add(classes[2]);
    // }else if(seatStatus === "3"){
    //     seatTaget.classList.add(classes[3]);
    // }
  }
}

function genPassengerDataList(psdData) {
  var psList = "";
  const statusText = [
    "<span class='text-secondary'>ว่าง<span>",
    "<span class='text-info'>ยังไม่ขึ้นรถ<span>",
    "<span class='text-success'>ผู้โดยสารขึ้นรถ/ตรวจสอบแล้ว<span>",
    "<span class='text-danger'>ยกเลิก/ไม่มา<span>",
  ];
  const seatStatusImg = ["", "booked", "checked", "absent"];
  var ticketNoList = [];
  for (let index = 0; index < psdData.length; index++) {
    if (psdData[index].seat_lock === "0") {
      var peNone = null;
    } else {
      var peNone = null;
    }
    if (psdData[index].seat_ticket === "0") {
      var tikNo = "-";
    } else {
      var tikNo = psdData[index].seat_ticket;
    }

    var seatLock = psdData[index].seat_lock;
    var seatRatetype = psdData[index].seat_ratetype;
    var seatLadyseat = psdData[index].seat_ladyseat;

    // if(seatLock === "1"){
    //     var seatStatus = seatStatusImg[2];
    // }else if(seatRatetype === null){
    //     var seatStatus = seatStatusImg[0];
    // }else if(seatLock === "0" && seatRatetype === "1"){
    //     var seatStatus = seatStatusImg[1];
    // }else if(seatLock === "0" && seatRatetype === "2"){
    //     var seatStatus = seatStatusImg[1];
    // }else{
    //     var seatStatus = seatStatusImg[3];
    // }

    if (seatLadyseat == "1") {
      var ladyseatBg = 'style="background-color:#fec2d8;"';
      //console.log(seatLadyseat);
    } else {
      var ladyseatBg = "";
    }

    var seatLock = psdData[index].seat_lock;
    var seatRatetype = psdData[index].seat_ratetype;
    var seatLadyseat = psdData[index].seat_ladyseat;
    var seatTypenm = psdData[index].seat_typenm;
    var seatTypeid = psdData[index].seat_typeid;

    var passengerType = checkPassengerType(seatTypeid, seatRatetype);
    var seatStatus = checkSeatStatus(seatTypenm, seatLock);
    //console.log(psdData[index].seat_name);
    //console.log("seatTypeid :" + seatTypeid +" | seatRatetype : "+ seatRatetype);
    //console.log("passengerType :" + passengerType +" | seatStatus : "+ seatStatus);

    if (seatStatus === "empty") {
      var peNone = "peNone";
    } else {
      var peNone = "";
    }

    psList += `<li class="list-group-item passengerList ${peNone} ${passengerType}"  ${ladyseatBg} onClick='getSeatData("${
      psdData[index].seat_name
    }")'>
            <div class="row">
            <div class="col-2 seat empty ${seatStatus} d-flex flex-row justify-content-center">
                <p class='seatNList text-dark'>${psdData[index].seat_name}</p>
            </div>
            <div class="col-10 d-flex flex-column align-items-start justify-content-center">
            <table class="passengerDetailTalbe w-100">
            <tr>
                <td colspan='2' class="font-weight-bold text-right">หมายเลขตั๋ว : ${tikNo}</td>
            </tr>
            <tr>
                <td>สถานะ : </td>
                <td> ${checkPassengerSatatus(seatLock, seatRatetype)}</td>
            </tr>
            <tr>
                <td>ชื่อ ผดส. : </td>
                <td> ${psdData[index].seat_dspnm}</td>
            </tr>
            <tr>
                <td>ต้นทาง : </td>
                <td> ${psdData[index].seat_srcnm}</td>
            </tr>
            <tr>
                <td>จุดลงรถ : </td>
                <td> ${psdData[index].seat_desnm}</td>
            </tr>
            <tr>
                <td>ประเภท : </td>
                <td> ${psdData[index].seat_typenm}</td>
            </tr> 
            </table>
            </div>
            <div class="col-12 text-center">${psdData[index].seat_remark}</div>
            </div>
        </li>`;
    ticketNoList.push(psdData[index].seat_ticket);
  }
  //console.log(JSON.stringify(ticketNoList));
  document.getElementById("passengerList").innerHTML = psList;
}

function countSeatDivs() {
  const seatDivs = document.querySelectorAll(".seat");
  fireAlertToast("This bus has " + seatDivs.length + " Seats");
  console.log(seatDivs.length);
}

function handleScriptLoad() {
  return new Promise((resolve) => {
    var loadingIndicator = document.getElementById("loading-indicator");
    // Apply CSS transition to make it fade out gradually
    loadingIndicator.style.transition = "opacity 0.5s";
    // Set the opacity to 0 to initiate the fade-out effect
    loadingIndicator.style.opacity = "0";
    // After the fade-out animation completes, hide the loading indicator
    setTimeout(function () {
      loadingIndicator.style.display = "none";
      resolve(); // Resolve the promise once the loading indicator is hidden
    }, 500); // 500 milliseconds = 0.5 seconds

    // Any other code you want to execute after the script has loaded
  });
}

function showLoadInt() {
  return new Promise((resolve) => {
    var loadingIndicator = document.getElementById("loading-indicator");
    // Apply CSS transition to make it fade out gradually
    loadingIndicator.style.transition = "opacity 0s";
    // Set the opacity to 0 to initiate the fade-out effect
    loadingIndicator.style.opacity = "1";
    // After the fade-out animation completes, hide the loading indicator
    setTimeout(function () {
      loadingIndicator.style.display = "";
      resolve(); // Resolve the promise once the loading indicator is hidden
    }, 0); // 500 milliseconds = 0.5 seconds
  });
}

function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function changeView(view) {
  return;
  if (view === "overviewBtn") {
    var targetToShow = document.getElementById("busTemplate");
    var targetToHidden = document.getElementById("passengerDetails");
  } else if (view === "passengerDetailsBtn") {
    var targetToHidden = document.getElementById("busTemplate");
    var targetToShow = document.getElementById("passengerDetails");
  }
  targetToHidden.setAttribute("hidden", "");
  targetToShow.removeAttribute("hidden");
}

function intSwiper() {
  const swiper = new Swiper(".swiper", {
    // Optional parameters
    direction: "horizontal",
    loop: false,
    allowTouchMove: false,
    watchOverflow: false,
    // effect : 'coverflow',
    slidesPerView: "auto",
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
    },

    //Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    zoom: {
      maxRatio: 3,
      minRatio: 1,
    },

    // And if we need scrollbar
    // scrollbar: {
    //     el: '.swiper-scrollbar',
    // },
  });

  const slideButtons = document.querySelectorAll(".slide-button");

  slideButtons.forEach((button) => {
    button.addEventListener("click", function () {
      const slideIndex = parseInt(this.getAttribute("data-slide"));
        // Remove btn-secondary class from all buttons
        slideButtons.forEach((btn) => {
          btn.classList.remove("btn-secondary");
        });

        // Add btn-secondary class to the clicked button
        this.classList.add("btn-secondary");

        // Remove btn-info class from the clicked button
        this.classList.remove("btn-info");

         // Remove btn-info class from the other buttons
         slideButtons.forEach((btn) => {
          if (btn !== this) {
            btn.classList.add("btn-info");
          }
        });
      swiper.slideTo(slideIndex, 300, false); // 300ms duration for the slide transition
    });
  });


  return swiper;
}

async function readBarcode() {
  var { value: ticketNo } = await Swal.fire({
    text: "สแกนหรือกรอกเลขที่ตั๋ว",
    input: "number",
    inputAutoTrim: "true",
    inputAttributes: { autocomplete: "off", required: "true" },
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    showCancelButton: true,
    //reverseButtons : true,
    inputValidator: (value) => {
      if (!value) {
        return "โปรดสแกนหรือกรอกเลขที่ตั๋ว";
      }
    },
  });
  if (ticketNo !== "0" && ticketNo !== undefined) {
    //console.log(ticketNo);
    findPassengerByTiketNo(ticketNo);
  } else {
    //fireAlert("หมายเลขตั๋วไม่ถูกต้อง");
  }
  document.getElementById("tnInoutF").value = '';
}

async function closeTaxInput() {
  var { value: closeTaxNo } = await Swal.fire({
    text: "กรอกเลขแท็กซ์ปิดงาน",
    input: "number",
    inputAutoTrim: "true",
    inputAttributes: { autocomplete: "off", required: "true" },
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    showCancelButton: true,
    //reverseButtons : true,
    inputValidator: (value) => {
      if (!value) {
        return "โปรดกรอกเลขแท็กซ์ปิดงาน";
      }else{
        try {
            fireTaxS("ปิดงานสำเร็จ Tax No : " + value);
            //throw new Error("I JUST WANT IT TO ERROR LOL!"); //uncomment this to check what happens when error is encountered
        } catch (error) {
            fireTaxErr("ไม่สามารถบันทึกข้อมูลได้<br>โปรดลองอีกครั้ง");
        }
      }
    },
  });
}

function findPassengerByTiketNo(ticketNo) {
  console.log("Barcode Reader : "+ ticketNo);
  var ticketNo = ticketNoLength(ticketNo);
  if(ticketNo == null){
    fireAlert("โปรดสแกนบาร์โค้ดอีกครั้ง","pressBtnScanWitnOutBarcode");
    return
  }

  var passengerData = JSON.parse(sessionStorage.getItem("passengerData"));
  try {
    var psdTaget = passengerData.filter((x) => x.seat_ticket === ticketNo)[0]; // psd is short version of passengerData
    console.log(psdTaget.length);
    if(psdTaget.seat_lock === "1"){
      console.log("passengerAlreadyHere");
      passengerAlreadyHere(`${psdTaget.seat_name} : ${psdTaget.seat_dspnm}<br>ผู้โดยสารขึ้นรถแล้ว`);      
      return
    }
    getSeatData(psdTaget.seat_name);
  } catch (error) {
    console.log(error);
    fireAlert("ไม่พบเลขที่ตั๋ว ที่ตรงกับผู้โดยสารของรถคันนี้",error);
    logToServer("findPassengerByTiketNo->ไม่พบเลขที่ตั๋ว ที่ตรงกับผู้โดยสารในเที่ยวนี้",error)
  }
  document.getElementById("busTemplate").focus();
}

function findPassengerByTiketNoCamera(ticketNo) {
  console.log("Camera Reader : "+ ticketNo);
  var ticketNo = ticketNoLength(ticketNo);
  var passengerData = JSON.parse(sessionStorage.getItem("passengerData"));
  //console.log(passengerData);
  try {
    var psdTaget = passengerData.filter((x) => x.seat_ticket === ticketNo)[0]; // psd is short version of passengerData
    if(psdTaget.seat_lock === "1"){
      console.log("passengerAlreadyHere");
      passengerAlreadyHere(`${psdTaget.seat_name} : ${psdTaget.seat_dspnm}<br>ผู้โดยสารขึ้นรถแล้ว`); 
      return
    }
    getSeatData(psdTaget.seat_name);
  } catch (error) {
    console.log(error);
    fireAlert("ไม่พบเลขที่ตั๋ว ที่ตรงกับผู้โดยสารของรถคันนี้",error);
    logErrorToServer("findPassengerByTiketNoCamera",error)
  }
  document.getElementById("busTemplate").focus();
}

function forgetPassword() {
  Swal.fire({
    icon: "info",
    title: "ลืมรหัสผ่านหรอ?",
    text: "นึกให้ออกนะ สู้ๆ",
    confirmButtonText: "ครับพรี่",
  });
}

function convertTime(inputTime) {
  // Convert the input time to a string for easy manipulation
  const timeString = inputTime.toString();

  // Pad the time string with leading zeros to ensure it has at least 4 characters
  const paddedTimeString = timeString.padStart(4, "0");

  // Extract hours and minutes from the padded time string
  const hours = paddedTimeString.substr(0, 2);
  const minutes = paddedTimeString.substr(2, 2);

  // Return the formatted time in "hh:mm" format
  return `${hours}:${minutes}`;
}

async function getTimeTable() {
  const kn_date = moment(document.getElementById("kn_date").value)
    .locale("th")
    .add(543, "year")
    .format("DD/MM/YY");
  //let stpoint = stpoint;
  let topoint = document.getElementById("desPoint").value;
  // document.getElementById("dtopoint").innerHTML = topoint;
  // document.getElementById("d_date").innerHTML = kn_date;
  //let outlet = outlet;
  sessionStorage.setItem("stpoint", stpoint); //for tax 
  sessionStorage.setItem("topoint", topoint); //for tax
  let url = `API/proxy.php?method=getTimetable&kn_date=${kn_date}&stpoint=${stpoint}&topoint=${topoint}&outlet=${outlet}`;
  console.log(url);
  //fireAlertToast(url);
  try {
    toastLoading("กำลังเรียกข้อมูลตารางเดินรถ...");
    var response = await fetch(url);
    var jsonData = await response.json();
    var busTimeTalbe = jsonData;
    console.log(busTimeTalbe);
    toastSuccess("เรียกข้อมูลตารางเดินรถสำเร็จ");
    var html = "";
    for (let index = 0; index < busTimeTalbe.length; index++) {
      //console.log(busTimeTalbe[index].v_srctime);
      // const v_srctime = moment(busTimeTalbe[index].v_srctime.replace(/(\d{2})(\d{2})/, "$1:$2");, "hss").format("h:ss");
      // const v_srctime_2 = moment(busTimeTalbe[index].v_srctime.replace(/(\d{2})(\d{2})/, "$1:$2"), "hss").format("h:ss");
      // const v_toarrivetime = moment(busTimeTalbe[index].v_toarrivetime.replace(/(\d{2})(\d{2})/, "$1:$2"), "hss").format("h:ss");
      // console.log(v_srctime);
      html += `
        <tr onclick='busView("${busTimeTalbe[index].v_busline}","${
        busTimeTalbe[index].v_buslinetype
      }","${kn_date}","${
        busTimeTalbe[index].v_intsrctime
      }","${outlet}","${stpoint}","${busTimeTalbe[index].v_bustypename}","${busTimeTalbe[index].v_bustype}");'>
            <td>${busTimeTalbe[index].v_buslinecode}</td><td>${
        busTimeTalbe[index].v_bustypename
      }</td>
            <td>${convertTime(
              busTimeTalbe[index].v_intsrctime
            )}</td><td>${convertTime(
        busTimeTalbe[index].v_intleavetime
      )}</td><td>${convertTime(busTimeTalbe[index].v_toarrivetime)}</td>
        </tr>
        `;
    }
    document.getElementById("timetableDisplay").removeAttribute("hidden");
    document.getElementById("timetableBody").innerHTML = html;
  } catch (error) {
    console.log(error);
    fireAlertToast(
      "ไม่สามารถเรียกข้อมูลตารางเดินรถได้<br>โปรดลองอีกครั้งในภายหลัง"
    );
    logErrorToServer("getTimeTable",error)
  }
}

async function busView(
  busline,
  buslinetype,
  date,
  srctime,
  outlet,
  stpoint,
  busname,
  bustype
) {
  showLoadInt();
  let bUrl = "busView.php?";
  let restUrl = `busline=${busline}&buslinetype=${buslinetype}&date=${date}&srctime=${srctime}&outlet=${outlet}&stpoint=${stpoint}&busname=${busname}&bustype=${bustype}`;
  console.log(bUrl + restUrl);
  window.location.href = bUrl + restUrl;
}

function addMarginTopAvoid() {
  var bottomMenu = document.getElementById("bottomMenu");
  var offsetHeight = bottomMenu.offsetHeight;
  var allMarginTopElements = document.querySelectorAll(".mtb");
  console.log(allMarginTopElements);
  
  for (let index = 0; index < allMarginTopElements.length; index++) {
    allMarginTopElements[index].style.cssText = `margin-top: ${offsetHeight}px;`;
  }
  
}

function noTaxNumberStart() {
  console.log("noTaxNumberStart");
  setFormHiddenValue();
  document.getElementById("taxDes").setAttribute("readOnly", true);
  document.getElementById("taxInBetween").setAttribute("readOnly", true);
  // document.getElementById("sbp").removeAttribute("hidden");
  // document.getElementById("sbn").removeAttribute("hidden");
  document.getElementById("noTaxBtn").setAttribute("hidden", true);
  document.getElementById("haveTaxBtn").setAttribute("hidden", true);
  sessionStorage.setItem("isTaxSet", "1");
  // document.getElementById("sbn").removeAttribute("hidden");
  document.getElementById("scanBtn").removeAttribute("hidden");
  document.getElementById("bottomMenu").removeAttribute("hidden");

  sessionStorage.setItem("numberOftaxDes", "0"); //เลขแท๊กซ์ใบแรกลงปลายทาง
  sessionStorage.setItem("numberOftaxInBetween", "0"); //เลขแท๊กซ์ใบแรกลงระหว่างทาง
  document.getElementById("closeTaxBtn").removeAttribute("hidden");
  document.getElementById("haveTaxBtn").setAttribute("hidden", true);
  //document.getElementById("backBtn").setAttribute("hidden", true);
 

  addMarginTopAvoid();
}

async function haveTaxNumberStart() {
  try {
    console.log("haveTaxNumberStart");

    var taxDes = document.getElementById("taxDes").value;
    var taxInBetween = document.getElementById("taxInBetween").value;

    //#$setPara = $bustype."#".$date."#".$busline."#".$buslinetype."#".$srctime."#".$outlet."#".$stpoint."#".$topoint."#".$empname."#".$empid."#".$tax1."#".$tax2;
  
    var busline = sessionStorage.getItem('busline');
    var buslinetype = sessionStorage.getItem('buslinetype');
    var date = sessionStorage.getItem('date');
    var srctime = sessionStorage.getItem('srctime');
    var user_empnm = sessionStorage.getItem('user_name');
    var empid = sessionStorage.getItem('staff_id')
    var outlet = sessionStorage.getItem('outlet');
    var stpoint = sessionStorage.getItem('stpoint');
    var topoint = sessionStorage.getItem('topoint');
    var bustype = sessionStorage.getItem('bustype');

    setFormHiddenValue();
    

    var apiendpoint = `API/proxy.php?method=saveFirstTagno&bustype=${bustype}&date=${date}&busline=${busline}&buslinetype=${buslinetype}&srctime=${srctime}&outlet=${outlet}&stpoint=${stpoint}&topoint=${topoint}&prm_empnm=${user_empnm}&empid=${empid}&tax1=${taxDes}&tax2=${taxInBetween}`;

    console.log(apiendpoint);

    var response = await fetch(apiendpoint);
    var jsonData = await response.json();
    var locksumid = jsonData[0].par_locksumid;

    sessionStorage.setItem("locksumid",locksumid);

    //sessionStorage.setItem("locksumid",123);

    document.getElementById("taxDes").setAttribute("readOnly", true);
    document.getElementById("taxInBetween").setAttribute("readOnly", true);
    // document.getElementById("sbp").removeAttribute("hidden");
    // document.getElementById("sbn").removeAttribute("hidden");
    document.getElementById("noTaxBtn").setAttribute("hidden", true);
    document.getElementById("haveTaxBtn").setAttribute("hidden", true);
    sessionStorage.setItem("isTaxSet", "1");
    //document.getElementById("sbn").removeAttribute("hidden");
    document.getElementById("scanBtn").removeAttribute("hidden");
    document.getElementById("bottomMenu").removeAttribute("hidden");
    sessionStorage.setItem("numberOftaxDes", taxDes); //เลขแท๊กซ์ใบแรกลงปลายทาง
    sessionStorage.setItem("numberOftaxInBetween", taxInBetween); //เลขแท๊กซ์ใบแรกลงระหว่างทาง
    document.getElementById("closeTaxBtn").removeAttribute("hidden");
    document.getElementById("closeTaxBtn").removeAttribute("hidden");
    //document.getElementById("backBtn").setAttribute("hidden", true);

    document.getElementById("txt_tax1").value = taxDes;
    document.getElementById("txt_s_tax1").value = taxInBetween;

    addMarginTopAvoid();
  } catch (error) {
    console.log(error);
    fireAlert("ไม่สามารถดึงค่าเลขแท็กซ์ได้<br>โปรดลองอีกครั้ง",error);
    logToServer("haveTaxNumberStart",error);
  }
  
  
}

function isDepartureTimePassed(departTime, currentTime) {
  // Parse the time strings into Date objects
  const currentTimeObj = new Date(`2000-01-01T${currentTime}:00`);
  const departTimeObj = new Date(`2000-01-01T${departTime}:00`);

  // Calculate the time difference in milliseconds
  const timeDifference = currentTimeObj - departTimeObj;

  // Convert the time difference to minutes
  const timeDifferenceInMinutes = timeDifference / (1000 * 60);

  // Check if the time difference is greater than 5 minutes
  console.log(timeDifferenceInMinutes);
  return timeDifferenceInMinutes > 5;
}

function calDepartureTimePassed(departTime, currentTime) {
  // Parse the time strings into Date objects
  const currentTimeObj = new Date(`2000-01-01T${currentTime}:00`);
  const departTimeObj = new Date(`2000-01-01T${departTime}:00`);

  // Calculate the time difference in milliseconds
  const timeDifference = currentTimeObj - departTimeObj;

  // Convert the time difference to minutes
  const timeDifferenceInMinutes = timeDifference / (1000 * 60);

  // Check if the time difference is greater than 5 minutes
  console.log(timeDifferenceInMinutes);

  if(timeDifferenceInMinutes < 0){
    return 0;
  }

  return timeDifferenceInMinutes;
}

//4 พระ,5 ชีพราม,3 เด็ก seat_ratetype
function checkSeatStatus(seat_typenm, seat_lock) {
  //console.log("seat_typenm : "+ seat_typenm);
  //console.log("seat_lock : "+ seat_lock);

  var busDepartTime = sessionStorage.getItem("scrtimeConverted");
  var imaTime =  moment().format("H:ss");

  if (seat_typenm === "0" && seat_lock === "0") {
    //console.log("empty");
    return "empty";
  } else if (seat_typenm != "0" && seat_lock === "0" && isDepartureTimePassed(busDepartTime,imaTime)) {
    //console.log("booked");
    return "absentLate5mins";
  } else if (seat_typenm != "0" && seat_lock === "0") {
    //console.log("booked");
    return "booked";
  } else if (seat_typenm != "0" && seat_lock === "1") {
    //console.log("checked");
    return "checked";
  }
}

function checkPassengerType(seat_typeid, seat_ratetype) {
  if (seat_ratetype == "4") {
    console.log("soldier");
    //return "soldier";
  }

  if (seat_typeid == "4") {
    return "monk";
  } else if (seat_typeid == "5") {
    return "nb";
  } else if (seat_typeid == "3") {
    return "chiled";
  }

  return "";
}

function checkPassengerTypeText(seat_typeid) {
  if (seat_typeid == "4") {
    return "พระ";
  } else if (seat_typeid == "5") {
    return "ชีพราหมณ์";
  } else if (seat_typeid == "3") {
    return "เด็ก";
  }
}

function findDesPointName() {}
function checkTimePass() {}

function callFlutterScaner() {
  //window.flutter_inappwebview.callHandler('scanBarcode');
  //.flutter_inappwebview.callHandler('barcodeScanner', 'scanBarcode');
  window.flutterCommu.postMessage('scanbarcode');
}

function callFlutterCamera() {
  //window.flutter_inappwebview.callHandler('scanBarcode');
  //.flutter_inappwebview.callHandler('barcodeScanner', 'scanBarcode');
  window.flutterCommu.postMessage('takePhoto');
}

function recivePhoto(imgBase64){
  Swal.fire({
    html : `<img class='img-fluid' src="data:image/png;base64,${imgBase64}" alt="Photo" />`
  });
  //console.log(imgBase64);
}

async function remarkPassengerSatatus(ticketNo,remark,seatName,seatDspnm){
  var ticketNo = ticketNo;
  var user_outlet = sessionStorage.getItem('user_outlet');
  var user_empnm = sessionStorage.getItem('user_name');
  var apiendpoint = `API/proxy.php?method=saveRemarkLock&ticketno=${ticketNo}&remark=${remark}&user_outlet=${user_outlet}&prm_empnm=${user_empnm}`;
  console.log(apiendpoint);
  try {
    var response = await fetch(apiendpoint);
    var jsonData = await response.json();
    //console.log(jsonData);
    if(jsonData[0].log_success === "1"){
      resultRemarkPassenger("ผู้โดยสาร : " + remark + "<br>" + seatName + ' : ' + seatDspnm);
    }else{
      fireAlert("ไม่สามารถบันทึกหมายเหตุได้<br>โปรดลองอีกครั้ง",error);
      logToServer("remarkPassengerSatatus","cantSaveRemarkButAllInformationIsCorrect");
    }
  } catch (error) {
    console.log(error);
    logToServer("remarkPassengerSatatus",error);
  }
  loadBusData();
}

async function lockSeatPassenger(ticketNo,seatName,seatDspnm){
  var ticketNo = ticketNo;
  var busline = sessionStorage.getItem('busline');
  var buslinetype = sessionStorage.getItem('buslinetype');
  var date = sessionStorage.getItem('date');
  var srctime = sessionStorage.getItem('srctime');
  var user_empnm = sessionStorage.getItem('user_name');
  //http://203.146.21.210/ots/funcstaff.inc.php?method=saveLockBySeat&busline=20&buslinetype=2300&date=20/07/66&srctime=1&ticketno=00000001&prm_empnm=1
  var apiendpoint = `API/proxy.php?method=saveLockBySeat&busline=${busline}&buslinetype=${buslinetype}&date=${date}&srctime=${srctime}&ticketno=${ticketNo}&prm_empnm=${user_empnm}`;
  console.log(apiendpoint);

  try {
    var response = await fetch(apiendpoint);
    var jsonData = await response.json();
    console.log(jsonData);
    if(jsonData[0].st_turn === "1"){
      resultRemarkPassenger("ตรวจสอบผู้โดยสารแล้ว" + "<br>" + seatName + ' : ' + seatDspnm);
    }else{
      fireAlert("ไม่สามารถบันทึกสถานะของผู้โดยสารได้","cantSaveLockSeatButAllInformationIsCorrect");
    }
  } catch (error) {
    console.log(error);
    fireAlert("ไม่สามารถบันทึกข้อมูลได้<br>โปรดลองอีกครั้ง");
    logError("lockSeatPassenger",error);
  }
  loadBusData();
}

async function checkFirstTagno(){
  var busline = sessionStorage.getItem('busline');
  var buslinetype = sessionStorage.getItem('buslinetype');
  var date = sessionStorage.getItem('date');
  var srctime = sessionStorage.getItem('srctime');
  var user_outlet = sessionStorage.getItem('user_outlet');

  try {
    var apiendpoint = `API/proxy.php?method=checkFirstTagno&qdate=${date}&q_busline=${busline}&q_buslinetype=${buslinetype}&q_srctime=${srctime}&user_outlet=${user_outlet}`;
    
    console.log(apiendpoint);

    var response = await fetch(apiendpoint);
    var jsonData = await response.json();

    console.log(jsonData);

    if(jsonData[0].stag_des === null){
      document.getElementById("taxDes").value = 0;
    }else{
      document.getElementById("taxDes").value = jsonData[0].stag_des;
      document.getElementById("txt_tax1").value = jsonData[0].stag_des;
      document.getElementById("taxDes").setAttribute("disabled", "disabled");
    }

    if(jsonData[0].stag_own === null){
      document.getElementById("taxInBetween").value = 0;
    }else{
      document.getElementById("taxInBetween").value = jsonData[0].stag_own;
      document.getElementById("txt_s_tax1").value = jsonData[0].stag_own;
      document.getElementById("taxInBetween").setAttribute("disabled", "disabled");
    }

    if(jsonData[0].par_locksumid !== null){
      document.getElementById("txt_locksumid").value = jsonData[0].par_locksumid;
    }else{
      document.getElementById("txt_locksumid").value = 0;
    }

  } catch (error) {
    console.log(error);
    fireAlert("โปรดลองอีกครั้ง").then(function () {
      window.history.back();
    });
  }
}

function lossTimeMemory(){
    var busDepartTime = sessionStorage.getItem("scrtimeConverted");
    var imaTime =  moment().format("H:ss");
    document.getElementById("txt_miniter").value = calDepartureTimePassed(busDepartTime,imaTime);
    document.getElementById("txt_getCltim").value = calDepartureTimePassed(busDepartTime,imaTime);
    document.getElementById("txt_leavedatetime").value = calDepartureTimePassed(busDepartTime,imaTime);
    if(calDepartureTimePassed(busDepartTime,imaTime) > 0){
      document.getElementById("txt_comment").setAttribute("required", "true");
    }
}

async function sentCloseTaxFrom(event) {
  event.preventDefault();

  // Get form data
  const formElement = document.getElementById('frm');
      const formData = new FormData(formElement);

  // Convert form data to a plain object
  const formDataObject = {};
  formData.forEach((value, key) => {
    formDataObject[key] = value;
  });
  console.log( JSON.stringify(formDataObject));
  // Post data using Fetch API
  fetch('API/proxy.php?method=saveCloseJob', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json', // You can adjust the content type as needed
    },
    body: JSON.stringify(formDataObject),
  })
  .then(response => response.json())
  .then(data => {
    // Handle the response from the server if needed
    console.log(data);
    if (data[0].st_turn === "1"){
      fireTaxS(data[0].st_turn);
    }else{
      fireTaxErr(data[0].st_turn);
    }
  }
  )
  .catch(error => {
    // Handle errors if any
    console.error('Error:', error);
    fireTaxErr(error);
  });
}

function setFormHiddenValue() {
    document.getElementById('txt_busline').value = sessionStorage.getItem('busline');
    document.getElementById('txt_buslinetype').value = sessionStorage.getItem('buslinetype');
    document.getElementById('txt_date').value = sessionStorage.getItem('date');
    document.getElementById('txt_srctime').value = sessionStorage.getItem('srctime');
    document.getElementById('txt_busname').value = sessionStorage.getItem('busname');
    //document.getElementById('txt_leavedatetime').value = sessionStorage.getItem('busline');
    document.getElementById('txt_locksumid').value = sessionStorage.getItem('locksumid');
    //document.getElementById('txt_getCltim').value = sessionStorage.getItem('busline');
    document.getElementById('txt_empname').value = sessionStorage.getItem('user_name');
    document.getElementById('txt_empid').value = sessionStorage.getItem('staff_id');
    document.getElementById('txt_outlet').value = sessionStorage.getItem('outlet');
}

async function resultOfqueue(){
  var busline = sessionStorage.getItem('busline');
  var buslinetype = sessionStorage.getItem('buslinetype');
  var date = moment(sessionStorage.getItem('date'), "DD/MM/YY").subtract(43,'years').format("YY-MM-DD");
  var srctime = sessionStorage.getItem('srctime');
  var user_outlet = sessionStorage.getItem('user_outlet');
  var stpoint = sessionStorage.getItem('stpoint');



  try {

    //$url = "http://203.146.21.210/ots/funcstaff.inc.php?method=resultOfqueue&user_outlet="
    //.$user_outlet."&stpoint=".$stpoint."&q_busline=".$q_busline."&q_buslinetype=".$q_buslinetype."&qdate=".$qdate."&q_srctime=".$q_srctime."";

    var apiendpoint = `API/proxy.php?method=resultOfqueue&qdate=${date}&q_busline=${busline}&q_buslinetype=${buslinetype}&q_srctime=${srctime}&user_outlet=${user_outlet}&stpoint=${stpoint}`;
    
    console.log(apiendpoint);

    var response = await fetch(apiendpoint);
    var jsonData = await response.json();

    console.log(jsonData);

    document.getElementById("totalPassenger").innerHTML = jsonData[0].total_allseat;
    document.getElementById("passengerFrom").innerHTML = jsonData[0].total_uponbkk;
    document.getElementById("passengerPresent").innerHTML = jsonData[0].total_realseat;
    document.getElementById("passengerDown").innerHTML = jsonData[0].total_down;
    document.getElementById("passengerGoing").innerHTML = jsonData[0].total_otw;
    

  } catch (error) {
  console.log(error);
  }
}

function logToServer(action,message) {
  const url = 'API/proxy.php?method=errorLog';

  var staff_id = sessionStorage.getItem('staff_id');
  var username = sessionStorage.getItem('user_name');

  var logMessage = `[${staff_id}-${username} | ${action}] ${message}`;

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: `message=${encodeURIComponent(logMessage)}`
  };

  fetch(url, requestOptions)
    .then(response => {
      if (!response.ok) {
        throw new Error('Failed to log on the server.');
      }
      return response.text();
    })
    .then(data => {
      console.log(data); // Logs the response from the server
    })
    .catch(error => {
      console.error('Error logging to server:', error);
    });
}


